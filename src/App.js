import React, { useState } from 'react';
import Login from './Login';

function App() {
  const [loggedInUser, setLoggedInUser] = useState(null);

  const handleLogin = (username) => {
    setLoggedInUser(username);
  };

  return (
    <div className="App">
      {loggedInUser ? (
        <div>
          <h1>Welcome, {loggedInUser}!</h1>
          <button onClick={() => setLoggedInUser(null)}>Logout</button>
        </div>
      ) : (
        <Login onLogin={handleLogin} />
      )}
    </div>
  );
}

export default App;
